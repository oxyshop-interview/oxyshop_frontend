# oXyshop interview - frontend part
Provides frontend and controller for simple user registration. It uses oxyshop_api (https://gitlab.com/sceptri/oxyshop_api) to save and retrieve users from the database.

Needed software:
```
PHP 7.2.5 or higher
Composer
Mariadb or MySQL or other
```


## Installing
Copy the `oxyshop_frontend` to location of your choosing.

Create database for `oxyshop_frontend`.

> Start mysql or mariadb service (for example `systemctl start mysql`) if not running

> You will need to create database for *both* oxyshop_api and oxyshop_frontend

Open `.env` and change `DATABASE_URL` to your server, port, username, password and database name.

Run `oxyshop_api` (more in oxyshop_api git page) and write its address to `src/Constants.php` variable `API_URL`. 

Then open terminal in this folder and run `composer require symfony/runtime` and then start the server with `symfony server:start`.

> In case of session handler error, give a shot to: `sudo chown <your_user_name> /var/lib/php7`

Enjoy!